﻿package 
{
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import mx.core.ButtonAsset;
	
	import flash.html.HTMLLoader;
	import flash.events.HTMLUncaughtScriptExceptionEvent;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.events.MouseEvent;
    import flash.events.Event;
	
	// This is from the swc in the lib
	import fl.controls.Button;
	
	public class Main extends Sprite 
	{
		private var targetHeight:Number = 400;
		private var targetWidth:Number = 550;		
		private var theBrowser:HTMLLoader;
		private var AbstractDataLoader:URLLoader = new URLLoader();
		private var mapOrigin:Object;
		private var marker:Button;
		
		public function Main():void 
		{
			AbstractDataLoader.addEventListener(Event.COMPLETE, HandleData, false, 0, true);
				  
			createHtmlViewer();
		}
		
		private function createHtmlViewer():void {
			theBrowser = new HTMLLoader();
			
			// If set to True wont load external scripts in html
			theBrowser.placeLoadStringContentInApplicationSandbox = false;
			
			theBrowser.addEventListener(Event.COMPLETE, htmlLoaded, false, 0, true);
			theBrowser.addEventListener(HTMLUncaughtScriptExceptionEvent.UNCAUGHT_SCRIPT_EXCEPTION, scriptException, false, 0, true);
			
			theBrowser.paintsDefaultBackground = false;
			addChild(theBrowser);
			
			// These direct where function calls from Javascript should go
			theBrowser.window.JSnewGeocodeData = JSnewGeocodeData;
			theBrowser.window.JSgeocodeError = JSgeocodeError;
			theBrowser.window.JSnewDirectionData = JSnewDirectionData;
			theBrowser.window.JSnewDirectionDataError = JSnewDirectionDataError;
			
			// Set clickie to the top
			marker = new Button();
			marker.label = "Map Route";
			marker.x = 450;
			marker.y = 350;
			addChild(marker);
			marker.addEventListener(MouseEvent.CLICK, mapDirections);
			
			// Start loading the html
			AbstractDataLoader.load(new URLRequest("GoogleMapsV3_JSFunctions.html"));
			
		}

		private function htmlLoaded(e:Event):void {
		trace("------ HTML LOADED ------- ");
		// Remove the event
		e.currentTarget.removeEventListener(Event.COMPLETE, htmlLoaded);
			
			theBrowser.width = targetWidth;
			theBrowser.height = targetHeight;
		}

		private function scriptException(e:HTMLUncaughtScriptExceptionEvent):void {
			trace("ScriptException:" + e);
		}

		private function HandleData(e:Event):void {
			  e.currentTarget.removeEventListener(Event.COMPLETE, HandleData);

				var notXML:String = e.target.data;
		trace("Text loaded - rendering in browser");
				theBrowser.loadString(notXML);
		}

		private function JSnewGeocodeData(results:Object):void {
			trace("Mwa ha ha - AIR has the geocode data! " + results[0].geometry.location );
			mapOrigin = results;
		}

		private function JSgeocodeError(error:Object):void {
			trace("Geocoding error: " + error.toString() );	
		}

		//Result is a DirectionsRoute http://code.google.com/apis/maps/documentation/javascript/reference.html#DirectionsRoute
		private function JSnewDirectionData(_NewData:Object, _Copyright:Object, _Distance:Object, _Duration:Object):void {
			trace("New direction data raw: " + _NewData.toString() );
			try {
				
				for each(var route:Object in _NewData.routes[0].legs) {
					trace("Distance: " + route.distance.text + " Duration by Car: " + route.duration.text);
					 for (var i:int = 0; i < route.steps.length; i++) {
							 trace("Step: " + i + " - " + route.steps[i].instructions);
					 }
				}

			} catch(e:Error) {
				trace("Err: " + e.toString());
			}
			
			trace("New direction info - total distance is!!! " + _Distance + " duration by caa: " + _Duration);
			trace("New direction info - copyright is? " + _Copyright );
			
			theBrowser.window.setDirectionsOnMap(_NewData);
		}

		private function JSnewDirectionDataError(error:Object):void {
			trace("Direction error: " + error.toString() );	
		}

		private function mapDirections(e:MouseEvent):void {
			// Dewitt, Michigan
			// Turkey Hill Experience 301 Linden Street Columbia, PA 17512
			// National Museum of the Pacific War 340 East Main Street Fredericksburg, Texas 78624

			// -- GET DIRECTIONS FOR ENTIRE TRIP
			var tripArr:Array = new Array("National Museum of the Pacific War 340 East Main Street Fredericksburg, Texas 78624","Turkey Hill Experience 301 Linden Street Columbia, PA 17512","Dewitt, Michigan");
			theBrowser.window.calculateEntireRoute(tripArr);
		}
	}
	
}